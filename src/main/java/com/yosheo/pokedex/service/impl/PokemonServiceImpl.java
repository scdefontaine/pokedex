package com.yosheo.pokedex.service.impl;

import com.yosheo.pokedex.dbo.PokemonDBO;
import com.yosheo.pokedex.dto.PokemonDTO;
import com.yosheo.pokedex.repository.PokemonRepo;
import com.yosheo.pokedex.service.PokemonService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PokemonServiceImpl implements PokemonService {

    private final PokemonRepo repository;

    @Autowired
    public PokemonServiceImpl(final PokemonRepo repositoryIn) {
        this.repository = repositoryIn;
    }

    @Override
    public List<PokemonDTO> findAllPokemon() {
        return this.repository.findAll().stream().map(this::dboToDto).collect(Collectors.toList());
    }

    private PokemonDTO dboToDto(final PokemonDBO dbo) {
        PokemonDTO dto = new PokemonDTO();
        BeanUtils.copyProperties(dbo, dto);
        return dto;
    }
}
