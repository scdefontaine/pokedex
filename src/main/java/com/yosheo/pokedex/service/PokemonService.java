package com.yosheo.pokedex.service;

import com.yosheo.pokedex.dto.PokemonDTO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PokemonService {

    List<PokemonDTO> findAllPokemon();
}
