package com.yosheo.pokedex.controller;

import com.yosheo.pokedex.dto.PokemonDTO;
import com.yosheo.pokedex.service.PokemonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("pokemon")
public class PokemonController {

    private final Logger logger = LoggerFactory.getLogger(PokemonController.class);

    private PokemonService pokemonService;

    public PokemonController(final PokemonService pokemonServiceIn) {
        this.pokemonService = pokemonServiceIn;
    }

    @GetMapping()
    public List<PokemonDTO> findAllPokemon() {
        return this.pokemonService.findAllPokemon();
    }
}
