package com.yosheo.pokedex.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PokemonDTO implements Serializable {

    /**
     * serial num
     */
    private static final long serialVersionUID = 7676918116647226784L;

    private Integer id;

    private String name;

    private String type;
}
