package com.yosheo.pokedex.repository;

import com.yosheo.pokedex.dbo.PokemonDBO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PokemonRepo extends JpaRepository<PokemonDBO, Integer> {

    @Override
    List<PokemonDBO> findAll();
}
