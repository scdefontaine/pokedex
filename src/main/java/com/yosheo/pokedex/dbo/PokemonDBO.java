package com.yosheo.pokedex.dbo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "pokemon")
@Data
public class PokemonDBO {

    private static final long serialVersionUID = 1l;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;
}
