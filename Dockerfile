FROM maven:3.6.3-openjdk-11 as build

COPY . .
RUN mvn clean install
RUN echo "Maven Build Complete"

FROM openjdk:11-jre
COPY --from=build target/pokedex-0.0.1-SNAPSHOT.jar pokedex.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/pokedex.jar"]